PACKAGE := babble-curses
VERSION := 0.9
LICENSE := GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
AUTHOR := Mariano Lopez Minnucci <mlopezviedma@gmail.com>
ifndef PREFIX
  PREFIX = /usr
endif
LOCALEDIR := $(DESTDIR)/$(PREFIX)/share/locale
POTDIR := locale
SRCDIR := src
BUILDDIR := build
BINDIR := bin
LIBDIR := lib
DOCDIR := doc
PROGRAM := babble-curses
SRCEXT := cpp
VARS := -D PACKAGE=\"'$(PACKAGE)'\" -D PROGRAMVERSION=\"'$(VERSION)'\" -D LICENSE=\"'$(LICENSE)'\" -D AUTHOR=\"'$(AUTHOR)'\" -D LOCALEDIR=\"'$(LOCALEDIR)'\" -D PREFIX=\"'$(DESTDIR)/$(PREFIX)'\"
CC := g++ -v -std=c++11 $(VARS)
GZIP := gzip --force
POT := xgettext --package-name=$(PACKAGE) --package-version='Version $(VERSION)' --copyright-holder='$(LICENSE)' --output-dir=$(POTDIR) --output="$(PACKAGE).pot" $(SRCDIR)/*
MO := msgfmt --no-hash --statistics -c -v
POFILES := $(wildcard $(POTDIR)/*.po)
SOURCES := $(shell echo $(SRCDIR)/{babble-curses,Babble{Curses,RoomMessage},ConfigManager,md5,md5wrapper,Window,staticjson}.$(SRCEXT))
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))
TRANSLATIONS := $(patsubst %.po,%,$(POFILES))
LIB := -lpthread -lssh -lncursesw
INC := -I include

all: curses translations doc

.PHONY: clean

.FORCE:

curses: $(OBJECTS)
	@echo -e "\e[1;33m Linking $(PROGRAM)...\e[0m"
	@mkdir -p $(BINDIR)
	@echo -e "\e[1;32m $(CC) $^ -o $(BINDIR)/$(PROGRAM) $(LIB) $(LDFLAGS)\e[0m"; $(CC) $^ -o $(BINDIR)/$(PROGRAM) $(LIB) $(LDFLAGS)

doc: curses
	help2man -N $(BINDIR)/babble-curses -o $(DOCDIR)/babble-curses.1
	$(GZIP) $(DOCDIR)/babble-curses.1

translations: $(POFILES)

pot:
	@echo -e "\e[1;32m $(POT)\e[0m"; $(POT)

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(BUILDDIR)
	@echo -e "\e[1;32m $(CC) $(CFLAGS) $(INC) $(LDFLAGS) -c -o $@ $<\e[0m"; $(CC) $(CFLAGS) $(INC) $(LDFLAGS) -c -o $@ $<

%.po: .FORCE
	@echo -e "\e[1;32m @mkdir -p $(basename $@)/LC_MESSAGES\e[0m"
	@mkdir -p $(basename $@)/LC_MESSAGES
	@echo -e "\e[1;32m $(MO) -o $(basename $@)/LC_MESSAGES/$(PACKAGE).mo $@\e[0m"
	$(MO) -o $(basename $@)/LC_MESSAGES/$(PACKAGE).mo $@

install:
	install -Dv -m 755 $(BINDIR)/$(PROGRAM) $(DESTDIR)/$(PREFIX)/bin/$(PROGRAM)
	install -Dv -m 644 $(DOCDIR)/babble-curses.conf $(DESTDIR)/$(PREFIX)/share/babble-curses/babble-curses.conf
	install -Dv -m 644 $(DOCDIR)/HELP $(DESTDIR)/$(PREFIX)/share/babble-curses/HELP
	$(BINDIR)/$(PROGRAM) --version >> $(DESTDIR)/$(PREFIX)/share/babble-curses/HELP
ifneq ($(strip $(TRANSLATIONS)),)
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/locale
	cp -rv $(TRANSLATIONS) -t $(DESTDIR)/$(PREFIX)/share/locale
endif
ifneq ($(strip $(wildcard $(DOCDIR)/*.1.gz)),)
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/man/man1/
	install -Dv -m 644 $(DOCDIR)/*.1.gz $(DESTDIR)/$(PREFIX)/share/man/man1/
endif

clean:
	@echo -e "\e[1;31m Cleaning...\e[0m"; 
	@echo " $(RM) -r $(BUILDDIR) $(BINDIR) $(DOCDIR)/babble-curses.1.gz"
	$(RM) -r $(BUILDDIR) $(BINDIR) $(DOCDIR)/babble-curses.1.gz
