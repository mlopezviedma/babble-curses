/*
 * This file is part of Babble - sockets server for GNU/Linux systems,
 * with plugins support. Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "BabbleRoomMessage.h"

BabbleRoomMessage::BabbleRoomMessage()
{
}

BabbleRoomMessage::~BabbleRoomMessage()
{
}

void BabbleRoomMessage::staticjson_init(staticjson::ObjectHandler* h)
{
  h->add_property("timestamp", &timestamp);
  h->add_property("from", &from);
  h->add_property("to", &to);
  h->add_property("type", &type);
  h->add_property("content", &content);
  h->add_property("is_user", &is_user);
}
