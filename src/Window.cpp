﻿/*
 * This file is part of babble-curses - curses client for the Babble Server.
 * Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../include/Window.h"

const std::string Window::RESET = "\0110";
const std::string Window::BOLD = "\0112";
const std::string Window::ITALIC = "\0113";
const std::string Window::UNDERLINE = "\0114";
const std::string Window::NOTBOLD = "\0115";
const std::string Window::NOTITALIC = "\0116";
const std::string Window::NOTUNDERLINE = "\0117";
const std::string Window::RED = "\011r";
const std::string Window::GREEN = "\011g";
const std::string Window::YELLOW = "\011y";
const std::string Window::BLUE = "\011b";
const std::string Window::MAGENTA = "\011m";
const std::string Window::CYAN = "\011c";
const std::string Window::WHITE = "\011w";
const std::string Window::POSITIVE = "\011p";
const std::string Window::NEGATIVE = "\011n";

Window::Window(int id, int nlines, int ncols, int begin_y, int begin_x, bool has_border, int buffer_size)
{
  this->id = id;
  this->has_border = has_border;
  total_lines = nlines;
  total_cols = ncols;
  if (buffer_size >= 0) this->buffer_size = buffer_size; else this->buffer_size = 0;
  if (has_border) {
    lines = nlines - 2;
    cols = ncols - 2;
    border = newwin(nlines, ncols, begin_y, begin_x);
    ++begin_y;
    ++begin_x;
    box(border, 0, 0);
  }
  else {
    lines = nlines;
    cols = ncols;
  }
  window = newwin(lines, cols, begin_y, begin_x);
  cursor = 1;
  offset = 0;
  scroll = 0;
}

Window::~Window()
{
  if (has_border) delwin(border);
  delwin(window);
}

void Window::showWin()
{
  if (has_border) {
    box(border, 0, 0);
    wnoutrefresh(border);
  }
  clearok(window, true);
  wclear(window);
  int l, i, j;
  bool done = false;
  std::list<std::string>::iterator it = sized_content.end();
  for (int y = 0; y < lines; ++y)
    if (it == sized_content.begin()) break; else --it;
  for (int y = 0; y < scroll; ++y)
    if (it == sized_content.begin()) break; else --it;
  for (int y = 0; y < lines; ++y) {
    if (it == sized_content.end()) break;
    i = 0;
    j = 0;
    l = (*it).length();
    wmove(window, y, 0);
    while (i < l) {
      if ((*it)[i] == '\011') {
        wprintw(window, (*it).substr(j, i-j).c_str());
        if (i == (l-1)) {
          ++it;
          ++y;
          if ( (it == sized_content.end()) || (y == lines) ) {
            done = true;
            break;
          }
          i = 0;
          j = 0;
          l = (*it).length();
          wmove(window, y, 0);
        }
        else
          ++i;
        if ((*it)[i] == '0') wattrset(window, 0);
        else if ((*it)[i] == '1') wattrset(window, A_NORMAL);
        else if ((*it)[i] == '2') wattron(window, A_BOLD);
        else if ((*it)[i] == '3') wattron(window, A_ITALIC);
        else if ((*it)[i] == '4') wattron(window, A_UNDERLINE);
        else if ((*it)[i] == '5') wattroff(window, A_BOLD);
        else if ((*it)[i] == '6') wattroff(window, A_ITALIC);
        else if ((*it)[i] == '7') wattroff(window, A_UNDERLINE);
        else if ((*it)[i] == 'r') wattron(window, COLOR_PAIR(1));
        else if ((*it)[i] == 'g') wattron(window, COLOR_PAIR(2));
        else if ((*it)[i] == 'y') wattron(window, COLOR_PAIR(3));
        else if ((*it)[i] == 'b') wattron(window, COLOR_PAIR(4));
        else if ((*it)[i] == 'm') wattron(window, COLOR_PAIR(5));
        else if ((*it)[i] == 'c') wattron(window, COLOR_PAIR(6));
        else if ((*it)[i] == 'w') wattron(window, COLOR_PAIR(7));
        ++i;
        j = i;
      }
      else i++;
    }
    if (done) break;
    wprintw(window, (*it).substr(j, i-j).c_str());
    ++it;
  }
  clearok(window, true);
  wnoutrefresh(window);
}

void Window::refreshSize(int nlines, int ncols, int begin_y, int begin_x)
{
  total_lines = nlines;
  total_cols = ncols;
  delwin(window);
  if (has_border) {
    delwin(border);
    lines = nlines - 2;
    cols = ncols - 2;
    border = newwin(nlines, ncols, begin_y, begin_x);
    ++begin_y;
    ++begin_x;
    box(border, 0, 0);
  }
  else {
    lines = nlines;
    cols = ncols;
  }
  window = newwin(lines, cols, begin_y, begin_x);
  cursor = 1;
  offset = 0;
  scroll = 0;
  std::string line;
  sized_content.clear();
  for (std::list<std::string>::iterator it = content.begin(); it != content.end(); ++it) {
    line = (*it);
    if (line.length() == 0)
      sized_content.emplace_back(line);
    else {
      while (line.length() > cols) {
        sized_content.emplace_back(line.substr(0, cols));
        line = line.substr(cols);
      }
      if (line.length() > 0) {
        sized_content.emplace_back(line);
      }
    }
  }
  if (buffer_size > 0) while (sized_content.size() > buffer_size)
    sized_content.erase(sized_content.begin());
}


void Window::addLine(std::string line)
{
  content.emplace_back(line);
  if (buffer_size > 0) while (content.size() > buffer_size) content.erase(content.begin());
  if (line.length() == 0) {
    sized_content.emplace_back(line);
    if (scroll > 0) ++scroll;
  }
  else {
    while (line.length() > cols) {
      sized_content.emplace_back(line.substr(0, cols));
      line = line.substr(cols);
      if (scroll > 0) ++scroll;
    }
    if (line.length() > 0) {
      sized_content.emplace_back(line);
      if (scroll > 0) ++scroll;
    }
  }
  if (buffer_size > 0) while (sized_content.size() > buffer_size) sized_content.erase(sized_content.begin());
}

void Window::setBufferSize(int buffer_size)
{
  if (buffer_size >= 0) this->buffer_size = buffer_size;
  if (buffer_size > 0) while (content.size() > buffer_size) content.erase(content.begin());
}

void Window::clear() {
  content.clear();
  scroll = 0;
}

void Window::scrollUp(int n)
{
  if (sized_content.size() > lines) {
    scroll += n;
    if (scroll < 0) scroll = 0;
    if (scroll > sized_content.size() - lines) scroll = sized_content.size() - lines;
  }
}

int Window::getMaxScroll()
{
  int ret = sized_content.size() - lines;
  if (ret > 0) return ret; else return 0;
}
