﻿/*
 * This file is part of Babble - sockets server for GNU/Linux systems,
 * with plugins support. Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../include/ConfigManager.h"

void ConfigManager::loadConfig()
{
  std::string line, buffer;
  std::ifstream file;
  std::list<config_file_t> files;
  std::list<std::string> lines;
  md5wrapper md5;
  config_file_t config_file;
  config_file.path = configFilePath;
  config_file.md5 = md5.getHashFromFile(configFilePath);
  config_file.line = lines.begin();
  files.push_back(config_file); // Main config file
  std::list<config_file_t>::iterator itf = files.begin();
  std::list<config_file_t>::iterator itf_backup;
  std::list<std::string>::iterator itl;
  while (itf != files.end()) { // Read all config files in the files list
    itf_backup = files.begin();
    while (itf_backup != itf) { // Check if two files are identical
      if ((*itf_backup).md5 == (*itf).md5) break;
      ++itf_backup;
    }
    if (itf_backup != itf) {
      identicalSourcing.emit((*itf).path);
      ++itf;
      continue;
    }
    file.open((*itf).path.c_str());
    if (file.good())
      readingFile.emit((*itf).path);
    else {
      cannotReadFile.emit((*itf).path);
      ++itf;
      continue;
    }
    itl = lines.begin();
    while (itl != lines.end()) if (itl != (*itf).line) ++itl; else break;
    while (std::getline(file, line)) { // Add file lines to the lines list
      while (std::isspace(line[0])) line.erase(0, 1);
      if (line.length() == 0) continue;
      if (line[0] == '#') continue;
      while (line.back() == '\\') {
        std::getline(file, buffer);
        line = line.substr(0, line.length()-1) + buffer;
      }
      lines.insert(itl, line);
    }
    file.close();
    itl = lines.begin();
    while (itl != lines.end()) { // Add every "source" line to the files list
      if ((*itl).substr(0, 7) == "source ") {
        line = (*itl).substr(7, (*itl).length()-7);
        while (std::isspace(line[0])) line.erase(0, 1);
        (*itl) = "";
        config_file.path = line;
        config_file.md5 = md5.getHashFromFile(line);
        config_file.line = itl;
        files.push_back(config_file);
      }
      ++itl;
    }
    ++itf;
  }
  int i;
  config_variable_t var;
  std::list<config_variable_t> new_config;
  std::list<config_variable_t>::iterator itc;
  itl = lines.begin();
  while (itl != lines.end()) { // Process all lines
    if ((*itl).length() == 0) {
      ++itl;
      continue;
    }
    if ((*itl).substr(0, 6) == "unset ") {
      line = (*itl).substr(6, (*itl).length()-6);
      while (line.length() > 0) {
        i = 0;
        while (std::isspace(line[0])) line.erase(0, 1);
        while (i < line.length()) if (!std::isspace(line[i])) ++i; else break;
        buffer = line.substr(0, i);
        itc = new_config.begin();
        while (itc != new_config.end()) if (itc->name != buffer) ++itc; else break;
        if (itc != new_config.end())
          itc->value = "";
        else {
          var.name = buffer;
          var.value = "";
          new_config.push_back(var);
        }
        line = line.substr(i, line.length()-i);
      }
      ++itl;
      continue;
    }
    i = 0;
    while (i < (*itl).length()) if ((*itl)[i] != '=') ++i; else break;
    if (i == (*itl).length()) {
      while ((*itl).length() > 0)
        if ((*itl)[(*itl).length()-1] == '+') (*itl).erase((*itl).length()-1, 1); else break;
      itc = new_config.begin();
      while (itc != new_config.end()) if (itc->name != (*itl).substr(0, i)) ++itc; else break;
      if (itc != new_config.end())
        itc->value = "true";
      else {
        var.name = (*itl).substr(0, i);
        var.value = "true";
        new_config.push_back(var);
      }
      ++itl;
      continue;
    }
    if ((*itl)[i-1] == '+') {
      while (i > 0)
        if ((*itl)[i-1] == '+') {
          (*itl).erase(i-1, 1);
          --i;
        }
        else break;
      itc = new_config.begin();
      while (itc != new_config.end()) if (itc->name != (*itl).substr(0, i)) ++itc; else break;
      if (itc != new_config.end()) {
        itc->value = itc->value + (*itl).substr(i+1, (*itl).length()-i-1);
      }
      else {
        var.name = (*itl).substr(0, i);
        var.value = (*itl).substr(i+1, (*itl).length()-i-1);
        new_config.push_back(var);
      }
    }
    else {
      if (i == (*itl).length()-1) {
        itc = new_config.begin();
        while (itc != new_config.end()) if (itc->name != (*itl).substr(0, i)) ++itc; else break;
        if (itc != new_config.end())
          itc->value = "";
        else {
          var.name = (*itl).substr(0, i);
          var.value = "";
          new_config.push_back(var);
        }
      }
      else {
        itc = new_config.begin();
        while (itc != new_config.end()) if (itc->name != (*itl).substr(0, i)) ++itc; else break;
        if (itc != new_config.end()) {
          itc->value = (*itl).substr(i+1, (*itl).length()-i-1);
        }
        else {
          var.name = (*itl).substr(0, i);
          var.value = (*itl).substr(i+1, (*itl).length()-i-1);
          new_config.push_back(var);
        }
      }
    }
    ++itl;
  }
  itc = new_config.begin();
  while (itc != new_config.end()) {
    setValue(itc->name, itc->value);
    ++itc;
  }
}

std::string ConfigManager::getValue(std::string name)
{
  std::list<config_variable_t>::iterator it = config.begin();
  while ( (it != config.end()) && (it->name != name) ) ++it;
  if (it != config.end()) return it->value; else return "";
}

void ConfigManager::setValue(std::string name, std::string value)
{
  std::list<config_variable_t>::iterator it = config.begin();
  while (it != config.end()) if (it->name != name) ++it; else break;
  if (it != config.end()) {
    if (it->value != value) {
      if (value != "") it->value = value; else config.erase(it);
      configChanged.emit(name, value);
    }
  }
  else {
    if (value != "") {
      config_variable_t variable;
      variable.name = name;
      variable.value = value;
      config.push_back(variable);
      configChanged.emit(name, value);
    }
  }
}

void ConfigManager::addToConfig(std::string name, std::string value)
{
  if (value != "") {
    std::list<config_variable_t>::iterator it = config.begin();
    while (it != config.end()) if (it->name != name) ++it; else break;
    if (it != config.end()) {
      it->value = it->value + value;
      value = it->value;
    }
    else {
      config_variable_t variable;
      variable.name = name;
      variable.value = value;
      config.push_back(variable);
    }
    configChanged.emit(name, value);
  }
}

void ConfigManager::removeConfig(std::string name)
{
  std::list<config_variable_t>::iterator it = config.begin();
  while (it != config.end()) if (it->name != name) ++it; else break;
  if (it != config.end()) {
    config.erase(it);
    configChanged.emit(name, "");
  }
}
