﻿/*
 * This file is part of babble-curses - curses client for the Babble Server.
 * Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../include/BabbleCurses.h"

BabbleCurses::BabbleCurses()
{
  setlocale(LC_ALL, "");
  bindtextdomain(PACKAGE, LOCALEDIR);
  textdomain(PACKAGE);
  port = 0;
  verbosity = SSH_LOG_NOLOG;
  if (getenv("HOME") != NULL)
    home = getenv("HOME");
  else
    home = "";
  if (getenv("EDITOR") != NULL)
    editor = getenv("EDITOR");
  else
    editor = "";
  version = "babble-curses " + std::string(gettext("Version")) + " "
    + std::string(PROGRAMVERSION) + "\n" + std::string(gettext("License")) + " " + std::string(LICENSE)
    + "\n" + std::string(gettext("Written by")) + " " + std::string(AUTHOR);
  help_file = std::string(PREFIX) + "/share/babble-curses/HELP";
  insert = false;
  ui = false;
  events_is_connected = false;
  requests_is_connected = false;
  bunch_of_events = false;
  do_exit = false;
  echo_events = false;
  echo_requests = false;
  connection_error = false;
  fixed_prompt = "> ";
  timeout = 10;
  ping = 10;
  delay = 250000;
  last_event = 0;
  titleWindow = new Window(0, 0, 0, 0, 0, false, 1);
  serverWindow = new Window(0, 0, 0, 0, 0, false);
  helpWindow = new Window(0, 0, 0, 0, 0, false);
  activeWindow = serverWindow;
  eventArrived.connect(this, &BabbleCurses::onEventArrived);
}

BabbleCurses::~BabbleCurses()
{
  delete serverWindow;
  delete titleWindow;
  delete helpWindow;
}

int BabbleCurses::exec(int argc, char **argv)
{
  std::string action, message;
  int i;
  std::string usage = "babble-curses [ --help | --usage | --version ]";
  for (int i = 1; i < argc; ++i) {
    if ( (strcmp(argv[i], "-h") == 0) || (strcmp(argv[i], "--help") == 0) ) action = "help";
    else if ( (strcmp(argv[i], "-u") == 0) || (strcmp(argv[i], "--usage") == 0) ) action = "usage";
    else if ( (strcmp(argv[i], "-v") == 0) || (strcmp(argv[i], "--version") == 0) ) action = "version";
    else action = "action_not_recognized " + std::string(argv[i]);
  }
  if (action == "help") {
    std::cout << usage << std::endl;
    std::cout << "  -h, --help            " << gettext("Show this help and exit.") << std::endl;
    std::cout << "  -u, --usage           " << gettext("Show usage and exit.") << std::endl;
    std::cout << "  -v, --version         " << gettext("Show the running version and exit.") << std::endl;
    std::cout << std::endl;
    std::cout << gettext("If no action is specified, execute the curses client.") << std::endl;
    std::cout << std::endl;
    std::cout << gettext("The client will read the configuration file \"~/.babble-curses\".") << std::endl;
    return 0;
  }
  else if (action == "usage") {
    std::cout << std::string(gettext("usage")) + ": " << usage << std::endl;
    return 0;
  }
  else if (action == "version") {
    std::cout << version << std::endl;
    return 0;
  }
  else if (action.substr(0, 22) == "action_not_recognized ") {
    std::cerr << gettext("Action not recognized") << ": " << action.substr(22) << std::endl;
    std::cerr << usage << std::endl;
    return 1;
  }
  startUI();
  helpWindow->title = std::string(Window::BOLD + gettext("Help Tab"));
  helpWindow->setBufferSize(buffer_size);
  std::ifstream file;
  file.open(help_file.c_str());
  if (file.good()) while (std::getline(file, message))
    if (!message.empty() && (message[0] != '#')) helpWindow->addLine(message);
  helpWindow->scrollUp(helpWindow->getMaxScroll());
  file.close();
  readConfig();
  if (host == "") host = "localhost";
  if (port <= 0) port = 22;
  if (login == "") {
    struct passwd* system_user = getpwuid(getuid());
    login = system_user->pw_name;
  }
  pthread_create(&input_thread, nullptr, &input, this);
  serverWindow->title = std::string(Window::BOLD + gettext("Server Tab") + Window::RESET + " ─ " + host);
  serverWindow->setBufferSize(buffer_size);
  serverWindow->addLine(std::string(
    Window::BOLD + Window::YELLOW + "F1/CTRL+Y: ") + Window::RESET + std::string(gettext("Open help tab")));
  serverWindow->addLine(std::string(
    Window::BOLD + Window::YELLOW + "F2/CTRL+S: ") + Window::RESET + std::string(gettext("Open server tab")));
  serverWindow->addLine(std::string(
    Window::BOLD + Window::YELLOW + "F3/CTRL+B: ") + Window::RESET + std::string(gettext("Go to previous room tab")));
  serverWindow->addLine(std::string(
    Window::BOLD + Window::YELLOW + "F4/CTRL+N: ") + Window::RESET + std::string(gettext("Go to next room tab")));
  serverWindow->addLine(std::string(
    Window::BOLD + Window::YELLOW + "CTRL+D/CTRL+Q: ") + Window::RESET + std::string(gettext("Disconnect and quit")));
  serverWindow->addLine(std::string(gettext("Host")) + ": " + host);
  serverWindow->addLine(std::string(gettext("Port")) + ": " + std::to_string(port));
  serverWindow->addLine(std::string(gettext("Login")) + ": " + login);
  printInfoMessage(replaceAllInstances(std::string(gettext("Connecting to host %1")), "%1", host) + "...");
  updateView();
  ssh_init();
  pthread_create(&events_thread, nullptr, &runEvents, this);
  while (!do_exit) {
    if (!requests_is_connected) {
      message = requestStdConnection();
      if (!connection_error) {
        events_socket = message.substr(0, message.find(" "));
        requests_socket = message.substr(message.find(" ")+1);
        runRequests();
        i = time(0);
        while (!events_is_connected) {
          usleep(delay);
          if (time(0) - i >= timeout) {
            printErrorMessage(std::string(gettext("Connection timeout")));
            break;
          }
        }
        if (events_is_connected) sleep(1);
        for (std::list<std::string>::iterator it = send_on_connect.begin(); it != send_on_connect.end(); ++it)
          sendRequest(*it, 0);
        for (std::list<Window*>::iterator it = tabs.begin(); it != tabs.end(); ++it) {
          (*it)->clear();
          sendRequest("Rooms join " + std::to_string((*it)->getID()), 0);
        }
      }
      else if ( (!events_is_connected) || (!requests_is_connected) ) {
        i = time(0);
        while ( (!do_exit) && (time(0) - i <= timeout) ) usleep(delay);
        continue;
      }
    }
    if (i >= ping*1000000/delay) {
      sendRequest("", 0);
      i = 0;
    }
    else ++i;
    if ( (ui) && (bunch_of_events) ) {
      if (time(0) - last_event > delay/1000000) {
        updateView();
        bunch_of_events = false;
      }
      else {
        titleWindow->showWin();
        doupdate();
      }
    }
    usleep(delay);
  }
  stopUI();
  pthread_cancel(events_thread);
  if (ssh_channel_is_open(requests_channel)) {
    ssh_channel_close(requests_channel);
    ssh_channel_free(requests_channel);
  }
  if (requests_session != nullptr) {
    ssh_disconnect(requests_session);
    ssh_free(requests_session);
  }
  ssh_finalize();
  return 0;
}

std::string BabbleCurses::requestStdConnection()
{
  std::string response, last_response;
  int nbytes;
  char c;
  ssh_session session = ssh_new();
  ssh_channel channel;
  if (session == nullptr) {
    printErrorMessage(std::string(gettext("Couldn't open SSH session")));
    connection_error = true;
  }
  else {
    ssh_options_set(session, SSH_OPTIONS_HOST, host.c_str());
    ssh_options_set(session, SSH_OPTIONS_USER, login.c_str());
    ssh_options_set(session, SSH_OPTIONS_LOG_VERBOSITY, &verbosity);
    ssh_options_set(session, SSH_OPTIONS_PORT, &port);
    if (ssh_connect(session) != SSH_OK) {
      printErrorMessage(replaceAllInstances(std::string(gettext("Couldn't connect to server %1")), "%1", host));
      connection_error = true;
    }
    else {
      if (ssh_userauth_password(session, nullptr, passwd.c_str()) != SSH_AUTH_SUCCESS) {
        printErrorMessage(replaceAllInstances(std::string(gettext("Couldn't authenticate on server %1")), "%1", host));
        connection_error = true;
      }
      else {
        printInfoMessage(std::string(gettext("Requesting a connection to the remote Babble Server")) + "...");
        if ((channel = ssh_channel_new(session)) == nullptr) return "";
        ssh_channel_open_session(channel);
        if (ssh_channel_request_pty(channel) != SSH_OK) return "";
        ssh_channel_request_exec(channel, "babble-client");
        ssh_channel_request_shell(channel);
        response = "request\n";
        ssh_channel_write(channel, response.c_str(), response.length());
        response = "";
        while (ssh_channel_is_open(channel) && !ssh_channel_is_eof(channel)) {
          nbytes = ssh_channel_read_nonblocking(channel, &c, 1, 0);
          if (nbytes < 0) break;
          if (nbytes > 0) {
            if (c == '\n') {
              last_response = response;
              response = "";
            }
            else response += c;
          }
        }
        ssh_channel_close(channel);
        ssh_channel_free(channel);
        connection_error = false;
      }
      ssh_disconnect(requests_session);
    }
    ssh_free(requests_session);
  }
  return last_response;
}

void* BabbleCurses::runEvents(void *This)
{
  int oldstate;
  pthread_setcancelstate(PTHREAD_CANCEL_ASYNCHRONOUS, &oldstate);
  BabbleCurses *me = static_cast<BabbleCurses *>(This);
  me->runEventsThread();
  pthread_exit(nullptr);
}

void* BabbleCurses::input(void *This)
{
  int oldstate;
  pthread_setcancelstate(PTHREAD_CANCEL_ASYNCHRONOUS, &oldstate);
  BabbleCurses *me = static_cast<BabbleCurses *>(This);
  me->inputThread();
  pthread_exit(nullptr);
}

void BabbleCurses::runRequests()
{
  if (ssh_channel_is_open(requests_channel)) {
    ssh_channel_close(requests_channel);
    ssh_channel_free(requests_channel);
  }
  if (requests_session != nullptr) {
    ssh_disconnect(requests_session);
    ssh_free(requests_session);
  }
  requests_session = ssh_new();
  ssh_options_set(requests_session, SSH_OPTIONS_HOST, host.c_str());
  ssh_options_set(requests_session, SSH_OPTIONS_USER, login.c_str());
  ssh_options_set(requests_session, SSH_OPTIONS_LOG_VERBOSITY, &verbosity);
  ssh_options_set(requests_session, SSH_OPTIONS_PORT, &port);
  ssh_connect(requests_session);
  ssh_userauth_password(requests_session, nullptr, passwd.c_str());
  requests_channel = ssh_channel_new(requests_session);
  ssh_channel_open_session(requests_channel);
  ssh_channel_request_pty(requests_channel);
  ssh_channel_request_exec(requests_channel, "babble-client");
  ssh_channel_request_shell(requests_channel);
  std::string message = "write " + requests_socket + "\n";
  ssh_channel_write(requests_channel, message.c_str(), message.length());
  message = "";
  char c;
  while (ssh_channel_is_open(requests_channel) && !ssh_channel_is_eof(requests_channel)) {
    ssh_channel_read_nonblocking(requests_channel, &c, 1, 0);
    if (c == '\n') break;
  }
  requests_is_connected = true;
}

void BabbleCurses::runEventsThread()
{
  std::string socket, event;
  char c;
  int nbytes;
  ssh_session session = ssh_new();
  ssh_options_set(session, SSH_OPTIONS_HOST, host.c_str());
  ssh_options_set(session, SSH_OPTIONS_USER, login.c_str());
  ssh_options_set(session, SSH_OPTIONS_LOG_VERBOSITY, &verbosity);
  ssh_options_set(session, SSH_OPTIONS_PORT, &port);
  while (!do_exit) {
    if (!requests_is_connected) {
      usleep(delay);
      continue;
    }
    printInfoMessage(std::string(gettext("Establishing a connection with the remote Babble Client")) + "...");
    ssh_connect(session);
    ssh_userauth_password(session, nullptr, passwd.c_str());
    ssh_channel channel = ssh_channel_new(session);
    ssh_channel_open_session(channel);
    ssh_channel_request_pty(channel);
    ssh_channel_request_exec(channel, "babble-client");
    ssh_channel_request_shell(channel);
    socket = events_socket;
    event = "read " + socket + "\n";
    ssh_channel_write(channel, event.c_str(), event.length());
    event = "";
    while (ssh_channel_is_open(channel) && !ssh_channel_is_eof(channel)) {
      nbytes = ssh_channel_read_nonblocking(channel, &c, 1, 0);
      if (c == '\n') break;
    }
    events_is_connected = true;
    while (ssh_channel_is_open(channel) && !ssh_channel_is_eof(channel)) {
      if (events_socket != socket) break;
      nbytes = ssh_channel_read_nonblocking(channel, &c, 1, 0);
      if (nbytes < 0) {
        events_is_connected = false;
        break;
      }
      else if (nbytes > 0) {
        if (c == '\n') {
          eventArrived.emit(event);
          event = "";
        }
        else event += c;
      }
      else
        usleep(delay);
    }
    ssh_channel_close(channel);
    ssh_channel_free(channel);
    ssh_disconnect(session);
  }
  ssh_free(session);
}

void BabbleCurses::sendRequest(std::string request, int tab_id)
{
  std::string full_request = request;
  std::string arg, temp_file, line;
  std::ifstream file;
  int nbytes;
  if ( (tab_id == 0) && (request == "help") ) {
    activeWindow = helpWindow;
    return;
  }
  if (tab_id != 0) {
    BabbleRoomMessage message;
    message.timestamp = 0;
    message.type = "plain";
    message.from = "";
    message.to = "";
    line = request;
    nextArg(line, arg);
    if (arg == "/quit") {
      request = "Rooms quit " + std::to_string(tab_id);
      sendRequest(request, 0);
      activeWindow = serverWindow;
      std::list<Window*>::iterator it;
      for (it = tabs.begin(); it != tabs.end(); ++it)
        if ((*it)->getID() == tab_id) {
          tabs.erase(it);
          break;
        }
      updateView();
      return;
    }
    if (arg == "/code") {
      if (editor == "") {
        line = gettext("No variable %1 found on your environment.");
        line = Window::BOLD + Window::RED + replaceAllInstances(line, "%1", "EDITOR") + Window::RESET;
        activeWindow->addLine(line);
        updateView();
        return;
      }
      nextArg(line, arg);
      if (arg.length() > 0) message.to = arg;
      request = "";
      message.type = "code";
      stopUI();
      char t[] = "/tmp/babble-cursesXXXXXX";
      temp_file = mktemp(t);
      line = editor + " " + temp_file;
      system(line.c_str());
      file.open(temp_file.c_str());
      if (!file.good()) {
        startUI();
        updateView();
        return;
      }
      while (std::getline(file, line)) request += line + "\r";
      file.close();
      remove(temp_file.c_str());
      startUI();
      updateView();
    }
    else if (arg == "/pm") {
      nextArg(line, arg);
      message.to = arg;
      request = line;
    }
    message.content = request;
    full_request = "Rooms send " + std::to_string(tab_id) + " " + staticjson::to_json_string(message);
  }
  nbytes = SSH_ERROR;
  if (ssh_channel_is_open(requests_channel) && !ssh_channel_is_eof(requests_channel)) {
    if ( echo_requests && (full_request.length() > 0) )
      serverWindow->addLine(Window::BOLD + "<< " + Window::RESET + full_request);
    full_request = full_request + "\n";
    nbytes = ssh_channel_write(requests_channel, full_request.c_str(), full_request.length());
  }
  if (nbytes == SSH_ERROR) {
    requests_is_connected = false;
  }
  updateView();
}

void BabbleCurses::onEventArrived(std::string event)
{
  if (echo_events)
    serverWindow->addLine(Window::BOLD + ">> " + Window::RESET + event);
  std::string arg1, arg2, arg3, line;
  int tab_id, pos, length;
  std::list<char> sequence;
  Window* tab;
  nextArg(event, arg1);
  if (arg1 == "Rooms") {
    nextArg(event, arg1);
    nextArg(event, arg2);
    if (arg1 != "error") {
      tab_id = std::stoi(arg2);
      std::list<Window*>::iterator it;
      for (it = tabs.begin(); it != tabs.end(); ++it)
        if ((*it)->getID() == tab_id) {
          tab = *it;
          break;
        }
      if (it == tabs.end()) {
        tab = new Window(tab_id, LINES-4, COLS, 2, 0, false, buffer_size);
        tab->title = Window::BOLD + gettext("Room") + " #" + arg2;
        tabs.push_back(tab);
      }
      if (arg1 == "message") {
        BabbleRoomMessage message;
        if (staticjson::from_json_string(event.c_str(), &message, nullptr) != 0) {
          if (message.content != "") {
            line = getTimeString(message.timestamp) + " ";
            if (message.type == "plain") {
              pos = 0;
              length = message.content.length();
              while (pos < length) {
                if (length - pos > 2) {
                  if (message.content.substr(pos, 3) == "/b[") {
                    message.content.replace(pos, 3, Window::BOLD);
                    length = message.content.length();
                    sequence.push_front('b');
                  }
                  else if (message.content.substr(pos, 3) == "/i[") {
                    message.content.replace(pos, 3, Window::ITALIC);
                    length = message.content.length();
                    sequence.push_front('i');
                  }
                  else if (message.content.substr(pos, 3) == "/u[") {
                    message.content.replace(pos, 3, Window::UNDERLINE);
                    length = message.content.length();
                    sequence.push_front('u');
                  }
                }
                if (length - pos > 0) {
                  if ( (message.content.substr(pos, 1) == "]") && (!sequence.empty()) ) {
                    if ((*sequence.begin()) == 'b')
                      message.content.replace(pos, 1, Window::NOTBOLD);
                    else if ((*sequence.begin()) == 'i')
                      message.content.replace(pos, 1, Window::NOTITALIC);
                    else if ((*sequence.begin()) == 'u')
                      message.content.replace(pos, 1, Window::NOTUNDERLINE);
                    length = message.content.length();
                    sequence.erase(sequence.begin());
                  }
                }
                ++pos;
              }
              if (message.content.find(login) != std::string::npos)
                message.content = Window::GREEN + message.content;
              message.content += Window::RESET;
            }
            if ( (message.type == "plain")
              && (message.content.substr(0, 4) == "/me ")
              && (message.to == "") ) {
                line += Window::ITALIC + message.from + " " + message.content.substr(4);
                tab->addLine(line);
                tab->last_message = line;
            }
            else {
              line += Window::BOLD;
              if (message.from == login) line += Window::YELLOW;
              line += message.from + Window::WHITE;
              if (message.to != "") {
                line += "->";
                if (message.to == login) line += Window::YELLOW;
                line += message.to;
              }
              line += Window::RESET;
              if (message.type == "code") {
                line = line + " (" + gettext("code snippet") + ")";
                tab->addLine(line);
                tab->last_message = line;
                while (message.content.length() > 0) {
                  pos = message.content.find("\r");
                  if (pos != std::string::npos) {
                    line = message.content.substr(0, pos);
                    tab->addLine(line);
                    message.content = message.content.substr(pos+1);
                  }
                  else {
                    tab->addLine(message.content);
                    message.content = "";
                  }
                }
              }
              else {
                message.content += Window::RESET;
                line += ": " + message.content;
                tab->addLine(line);
                tab->last_message = line;
              }
            }
          }
        }
      }
      else if (arg1 == "join") {
        nextArg(event, arg3);
        line = gettext("%1 joined this room.");
        line = Window::CYAN + replaceAllInstances(line, "%1", arg3) + Window::RESET;
        tab->addLine(line);
        tab->last_message = line;
      }
      else if (arg1 == "quit") {
        nextArg(event, arg3);
        line = gettext("%1 left this room.");
        line = Window::CYAN + replaceAllInstances(line, "%1", arg3) + Window::RESET;
        tab->addLine(line);
        tab->last_message = line;
      }
    }
  }
  if (time(0) - last_event < delay/1000000) {
    titleWindow->addLine(Window::BOLD + Window::RED +
      gettext("Receiving a bunch of events...") + Window::RESET);
    bunch_of_events = true;
  }
  else
    updateView();
  last_event = time(0);
}

void BabbleCurses::inputThread()
{
  wchar_t key;
  char low, high;
  int cols;
  std::list<Window*>::iterator it;
  Window* win;
  while (key = getch()) {
    cols = COLS - prompt.length();
    switch (key) {
      case 0: case 1: case 3: case 5: case 6: case 7: break;
        break;
      case 9: break; // Tab
      case 11: case 12: case 13: case 16: case 18: case 20: case 21: case 22:
      case 23: case 24: case 26: case 27: case 28: case 29: case 30: case 31:
      case 37: break; // %
      case KEY_F(1):
      case CTRL('y'):
        activeWindow = helpWindow;
        updateView();
        break;
      case KEY_F(2):
      case CTRL('s'):
        activeWindow = serverWindow;
        updateView();
        break;
      case KEY_F(3):
      case CTRL('b'):
        if (tabs.begin() == tabs.end()) break;
        for (it = tabs.begin(); it != tabs.end(); ++it)
          if ((*it) == activeWindow) break;
        if (it == tabs.begin()) it = tabs.end();
        --it;
        activeWindow = *it;
        break;
      case KEY_F(4):
      case CTRL('n'):
        if (tabs.begin() == tabs.end()) break;
        for (it = tabs.begin(); it != tabs.end(); ++it)
          if ((*it) == activeWindow) break;
        if (it == tabs.end())
          activeWindow = *tabs.begin();
        else {
          activeWindow = *(++it);
          if (it == tabs.end()) activeWindow = *tabs.begin();
        }
        break;
      case KEY_F(5):
      case KEY_RESIZE:
        windowResized(SIGWINCH);
        break;
      case CTRL('d'):
      case CTRL('q'):
        activeWindow = serverWindow;
        serverWindow->addLine(Window::BOLD + Window::RED +
          replaceAllInstances(std::string(gettext("Closing connection with host %1")), "%1", host) +
          "..." + Window::RESET);
        updateView();
        do_exit = true;
        return;
        break;
      case '\n':
        if (activeWindow->cmdline.length() > 0) {
          win = activeWindow;
          sendRequest(win->cmdline, win->getID());
          win->cmdline = "";
          win->cursor = 1;
          win->offset = 0;
        }
        updateView();
        break;
      case KEY_UP:
        break;
      case KEY_DOWN:
        break;
      case KEY_LEFT:
        if (activeWindow->cursor > 1) {
          if ( (activeWindow->cursor < 3) && (activeWindow->offset > 0) )
            --activeWindow->offset;
          else
            --activeWindow->cursor;
        }
        else
          if (activeWindow->offset > 0) --activeWindow->offset;
        break;
      case KEY_RIGHT:
        if (activeWindow->cursor+activeWindow->offset < (activeWindow->cmdline.length()+1)) {
          ++activeWindow->cursor;
          if (activeWindow->cursor > (cols-1)) {
            activeWindow->cursor = cols-1;
            ++activeWindow->offset;
          }
        }
        break;
      case KEY_BACKSPACE:
      case 0x7f:
        if (activeWindow->cursor > 1) {
          activeWindow->cmdline.erase(activeWindow->cursor+activeWindow->offset-2,1);
          if ( (activeWindow->cursor < 3) && (activeWindow->offset > 0) )
            --activeWindow->offset;
          else
            --activeWindow->cursor;
        }
        else if (activeWindow->offset > 0) {
          activeWindow->cmdline.erase(activeWindow->cursor+activeWindow->offset-2,1);
          --activeWindow->offset;
        }
        break;
      case KEY_DC:
        activeWindow->cmdline.erase(activeWindow->cursor-1,1);
        break;
      case KEY_IC:
        insert = 1 - insert;
        break;
      case KEY_HOME:
        activeWindow->cursor = 1;
        activeWindow->offset = 0;
        break;
      case KEY_END:
        if (activeWindow->cmdline.length() > (cols-2)) {
          activeWindow->cursor = cols-1;
          activeWindow->offset = activeWindow->cmdline.length()-cols+2;
        }
        else {
          activeWindow->cursor = activeWindow->cmdline.length() + 1;
          activeWindow->offset = 0;
        }
        break;
      case KEY_PPAGE:
        if (LINES > 10) {
          if (scroll == 0)
            activeWindow->scrollUp(activeWindow->getLines()-1);
          else
            activeWindow->scrollUp(scroll);
        }
        else
          activeWindow->scrollUp(1);
        break;
      case KEY_NPAGE:
        if (LINES > 10) {
          if (scroll == 0)
            activeWindow->scrollUp(-activeWindow->getLines()+1);
          else
            activeWindow->scrollUp(-scroll);
        }
        else
          activeWindow->scrollUp(-1);
        break;
      default:
        low = key & 0xff; high = (key>>8) & 0xff;
        if (activeWindow->cmdline.length() >= activeWindow->cursor+activeWindow->offset) {
          if (insert) activeWindow->cmdline.erase(activeWindow->cursor+activeWindow->offset-1,1);
          activeWindow->cmdline = activeWindow->cmdline.substr(0,
            activeWindow->cursor+activeWindow->offset-1) + low +
            activeWindow->cmdline.substr(activeWindow->cursor+activeWindow->offset-1);
        }
        else activeWindow->cmdline = activeWindow->cmdline + low;
        ++activeWindow->cursor;
        if (activeWindow->cursor > cols) {
          activeWindow->cursor = cols;
          ++activeWindow->offset;
        }
    }
    updateView();
  }
  return;
}

void BabbleCurses::startUI()
{
  initscr();
  if ( (LINES < 8) || (COLS < 8) ) {
    std::cerr << std::endl << gettext("Terminal too small! Quitting") << std::endl;
    do_exit = true;
  }
  raw();
  cbreak();
  noecho();
  start_color();
  init_pair(1, COLOR_RED, COLOR_BLACK);
  init_pair(2, COLOR_GREEN, COLOR_BLACK);
  init_pair(3, COLOR_YELLOW, COLOR_BLACK);
  init_pair(4, COLOR_BLUE, COLOR_BLACK);
  init_pair(5, COLOR_MAGENTA, COLOR_BLACK);
  init_pair(6, COLOR_CYAN, COLOR_BLACK);
  init_pair(7, COLOR_WHITE, COLOR_BLACK);
  keypad(stdscr, TRUE);
  refresh();
  input_window = newwin(1, COLS-1, LINES-1, 0);
  titleWindow->refreshSize(1, COLS-1, 0, 0);
  serverWindow->refreshSize(LINES-4, COLS, 2, 0);
  helpWindow->refreshSize(LINES-4, COLS, 2, 0);
  helpWindow->scrollUp(helpWindow->getMaxScroll());
  for (std::list<Window*>::iterator it = tabs.begin(); it != tabs.end(); ++it)
    (*it)->refreshSize(LINES-4, COLS, 2, 0);
  updateView();
  ui = true;
}

void BabbleCurses::stopUI()
{
  ui = false;
  delwin(input_window);
  endwin();
}

void BabbleCurses::updateView()
{
  if (!ui) return;
  prompt = activeWindow->prompt + fixed_prompt;
  activeWindow->showWin();
  std::string notification;
  int i;
  for (std::list<Window*>::iterator it = tabs.begin(); it != tabs.end(); ++it) {
    i = (*it)->getID();
    if ( (activeWindow->getID() == i) && (activeWindow->getScroll() == 0) )
      activeWindow->last_message = "";
    if ((*it)->last_message.length() > 0) {
      notification += Window::RESET + " | " + Window::BOLD + gettext("Room") + " #" +
        std::to_string(i) + ": " + Window::RESET + (*it)->last_message;
    }
  }
  i = activeWindow->getScroll();
  move(LINES-2, 0);
  if (i > 0) {
    attron(A_BOLD);
    attron(COLOR_PAIR(3));
    printw("↓↓↓");
  }
  attrset(0);
  hline(0, COLS);
  move(1, 0);
  if (i < activeWindow->getMaxScroll()) {
    attron(A_BOLD);
    attron(COLOR_PAIR(3));
    printw("↑↑↑");
  }
  attrset(0);
  hline(0, COLS);
  if ( connection_error && (!events_is_connected || !requests_is_connected) )
    titleWindow->addLine(Window::RESET + Window::BOLD + Window::RED +
      gettext("Lost conneciton to the server. Reconnecting...") + Window::RESET);
  else
    titleWindow->addLine(std::string(Window::RESET + activeWindow->title + notification).substr(0, COLS-1));
  titleWindow->showWin();
  refresh();
  wmove(input_window, 0, 0);
  wclrtoeol(input_window);
  if (!insert) wattron(input_window, A_BOLD);
  mvwprintw(input_window, 0, 0, prompt.c_str());
  wattroff(input_window, A_BOLD);
  wprintw(input_window, activeWindow->cmdline.substr(activeWindow->offset,
    activeWindow->cmdline.length()-activeWindow->offset).c_str());
  wmove(input_window, 0, prompt.length()+activeWindow->cursor-1);
  wrefresh(input_window);
}

void BabbleCurses::readConfig()
{
  ConfigManager *configManager = new ConfigManager(home + "/.babble-curses.conf");
  configManager->loadConfig();
  config = configManager->getConfig();
  delete configManager;
  for (std::list<config_variable_t>::iterator it = config.begin(); it != config.end(); ++it) {
    if ((*it).name == "host")
      host = (*it).value;
    else if ((*it).name == "port") {
      try {
        port = std::stoi((*it).value);
      }
      catch (const std::invalid_argument& ia) {
        printErrorMessage("port: " + std::string(gettext("Invalid value.")));
        port = 22;
      }
      if (port <= 0) {
        printErrorMessage("port: " + std::string(gettext("Invalid value.")));
        port = 22;
      }
    }
    else if ((*it).name == "login")
      login = (*it).value;
    else if ((*it).name == "passwd")
      passwd = (*it).value;
    else if ((*it).name == "sendOnConnect") {
      std::string request;
      send_on_connect.clear();
      request = (*it).value + "\\n";
      int pos;
      while (request.length() > 0) {
        pos = request.find("\\n");
        if (pos != std::string::npos) {
          send_on_connect.push_back(request.substr(0, pos));
          request = request.substr(pos+2);
        }
        else {
          send_on_connect.push_back(request);
          request = "";
        }
      }
    }
    else if ((*it).name == "bufferSize") {
      try {
        buffer_size = std::stoi((*it).value);
      }
      catch (const std::invalid_argument& ia) {
        printErrorMessage("bufferSize: " + std::string(gettext("Invalid value.")));
        buffer_size = 0;
      }
      if (buffer_size < 0) {
        printErrorMessage("bufferSize: " + std::string(gettext("Invalid value.")));
        buffer_size = 0;
      }
    }
    else if ((*it).name == "scroll") {
      try {
        scroll = std::stoi((*it).value);
      }
      catch (const std::invalid_argument& ia) {
        printErrorMessage("scroll: " + std::string(gettext("Invalid value.")));
        scroll = 0;
      }
      if (scroll < 0) {
        printErrorMessage("scroll: " + std::string(gettext("Invalid value.")));
        scroll = 0;
      }
    }
    else if ((*it).name == "ping") {
      try {
        ping = std::stoi((*it).value);
      }
      catch (const std::invalid_argument& ia) {
        printErrorMessage("ping: " + std::string(gettext("Invalid value.")));
        ping = 10;
      }
      if (ping < 1) {
        printErrorMessage("ping: " + std::string(gettext("Invalid value.")));
        ping = 10;
      }
    }
    else if ((*it).name == "timeout") {
      try {
        timeout = std::stoi((*it).value);
      }
      catch (const std::invalid_argument& ia) {
        printErrorMessage("timeout: " + std::string(gettext("Invalid value.")));
        timeout = 10;
      }
      if (timeout < 1) {
        printErrorMessage("timeout: " + std::string(gettext("Invalid value.")));
        timeout = 10;
      }
    }
    else if ((*it).name == "delay") {
      try {
        delay = std::stoi((*it).value);
      }
      catch (const std::invalid_argument& ia) {
        printErrorMessage("delay: " + std::string(gettext("Invalid value.")));
        delay = 250;
      }
      if (delay < 1) {
        printErrorMessage("delay: " + std::string(gettext("Invalid value.")));
        delay = 250;
      }
      delay *= 1000;
    }
    else if ((*it).name == "prompt")
      fixed_prompt = (*it).value;
    else if ((*it).name == "echoReceivedEvents") {
      if ( ((*it).value == "true") || ((*it).value == "yes") )
        echo_events = true;
      else
        echo_events = false;
    }
    else if ((*it).name == "echoSentRequests") {
      if ( ((*it).value == "true") || ((*it).value == "yes") )
        echo_requests = true;
      else
        echo_requests = false;
    }
  }
}

void BabbleCurses::printInfoMessage(std::string message)
{
  serverWindow->addLine(Window::GREEN + message + Window::RESET);
  updateView();
}

void BabbleCurses::printErrorMessage(std::string message)
{
  serverWindow->addLine(Window::BOLD + Window::RED + message + Window::RESET);
  updateView();
}

void BabbleCurses::windowResized(int SIG)
{
  stopUI();
  startUI();
}

std::string BabbleCurses::replaceAllInstances(std::string string, std::string grab, std::string replace)
{
  int n = 0;
  while ((n = string.find(grab, 0)) != std::string::npos)
    string.replace(n, grab.size(), replace);
  return string;
}

std::string BabbleCurses::getTimeString(time_t timestamp) {
  std::string ret = "[";
  time_t now = time(0);
  char time_date[32];
  char now_date[32];
  tm t = *localtime(&timestamp);
  strftime(&time_date[0], 32, "%F", &t);
  tm n = *localtime(&now);
  strftime(&now_date[0], 32, "%F", &n);
  if (std::string(time_date) != std::string(now_date)) ret += (std::string(time_date) + " ");
  if (t.tm_hour < 10) ret += "0";
  ret += (std::to_string(t.tm_hour) + ":");
  if (t.tm_min < 10) ret += "0";
  ret += (std::to_string(t.tm_min) + "]");
  return ret;
}

void BabbleCurses::nextArg(std::string &string, std::string &arg)
{
  arg = "";
  while (std::isspace(string[0])) string.erase(0, 1);
  while (string.length() > 0) {
    if (std::isspace(string[0])) {
      string.erase(0, 1);
      break;
    }
    else arg += string[0];
    string.erase(0, 1);
  }
}
