# babble-curses - Curses remote client for the Babble Server

## Compilation

To build the package, run `make`. This will compile the `babble-curses`
program together with the man page and all the available translations.

If you don't want to build all these, you can specify what you
want by giving the corresponding rules to `make`:

```
make curses
make translations
make doc
```

To install the package, run `make install` as administrator user.

The following variables can be passed to `make`:

- DESTDIR: An alternative root (`/`) directory for installation
- PREFIX: An alternative `/usr` directory

## Compilation dependencies

- GNU GCC C/C++ Compiler
- C/C++ Standard Library
- GNU Make
- NCurses
- LibSSH
- GNU Gettext: for generating available translations (optional)
- Help2Man: For generating main man pages (optional)
- GNU Zip: For compressing the documentation (optional)

## Translations

If you want to work with the package translations, first run `make pot`.
This will generate a translation template file `locale/babble-curses.pot`
with all the current translatable strings.

Next, generate the PO files (one for each language you want to translate to)
using `msginit`. For example:

```
msginit -l de_DE -i locale/babble-curses.pot -o locale/de_DE.po
```

Keep in mind that the chosen locale should be currently installed on your system.

In case the corresponding PO file already exists (in other words, you want to work
with an already existent translation, possibly out-dated), what you'll need to do
instead is update its contents to account for the changes made on the translatable
strings by a way of:

```
msgmerge --update locale/<locale>.po locale/babble-curses.pot
```

You can now edit this PO file manually or with any PO editor and send a
pull request to the package maintainer anytime you want for this translation
update to be available for everybody else.

## License

License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>

Written by Mariano López Minnucci <mlopezviedma@gmail.com>
