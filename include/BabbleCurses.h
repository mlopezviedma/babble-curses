﻿/*
 * This file is part of babble-curses - curses client for the Babble Server.
 * Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BABBLECURSES_H
#define BABBLECURSES_H

#define SIGSLOT_DEFAULT_MT_POLICY multi_threaded_local

#define CTRL(c) ((c) & 037)

#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <pwd.h>
#include <signal.h>
#include <string>
#include <string.h>
#include <libintl.h>
#include <iostream>
#include <pthread.h>
#include <libssh/libssh.h>
#include <cwchar>
#include <ncurses.h>
#include <clocale>
#include "sigslot.h"
#include "ConfigManager.h"
#include "BabbleRoomMessage.h"
#include "Window.h"

class BabbleCurses : public sigslot::has_slots<>
{
  public:
    BabbleCurses();
    ~BabbleCurses();
    int exec(int argc, char **argv);
    std::string requestStdConnection();
    static void* runEvents(void *This);
    static void* input(void *This);
    void runRequests();
    void runEventsThread();
    void sendRequest(std::string request, int tab_id);
    void onEventArrived(std::string event);
    void inputThread();
    void startUI();
    void stopUI();
    void updateView();
    void windowResized(int SIG);
    std::string replaceAllInstances(std::string string, std::string grab, std::string replace);
    static std::string getTimeString(time_t time);
    static void nextArg(std::string &string, std::string &arg);
    
    sigslot::signal1<std::string> eventArrived;

  protected:
    std::string version, help_file, fixed_prompt, prompt;
    int buffer_size, scroll, timeout, ping, delay, last_event;
    bool insert, ui, events_is_connected, requests_is_connected, bunch_of_events, do_exit, echo_events, echo_requests, connection_error;
    WINDOW *input_window;
    Window *titleWindow;
    Window *activeWindow;
    Window *serverWindow;
    Window *helpWindow;
    std::list<Window*> tabs;
    std::list<Window*> menus;
  private:
    int verbosity, rc, port;
    ssh_session requests_session;
    ssh_channel requests_channel;
    std::string home ,editor, host, login, passwd, events_socket, requests_socket;
    std::list<std::string> send_on_connect;
    std::list<config_variable_t> config;
    pthread_t events_thread, input_thread;
    void printInfoMessage(std::string message);
    void printErrorMessage(std::string message);
    void readConfig();
};

#endif // BABBLECURSES_H
