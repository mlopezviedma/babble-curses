﻿/*
 * This file is part of babble-curses - curses client for the Babble Server.
 * Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONFIGMANAGER_H
#define CONFIGMANAGER_H

#include <string>
#include <libintl.h>
#include <list>
#include <sigslot.h>
#include <fstream>
#include "md5wrapper.h"

struct config_variable_t {
  std::string name, value;
};

struct config_file_t {
  std::string path, md5;
  std::list<std::string>::iterator line;
};

class ConfigManager : public sigslot::has_slots<>
{
  public:
    ConfigManager(std::string file_path) { configFilePath = file_path; }
    void setConfigFilePath(std::string file_path) { configFilePath = file_path; }
    void loadConfig();
    std::list<config_variable_t> getConfig() const { return config; }
    std::string getValue(std::string name);
    void setValue(std::string name, std::string value);
    void setValue(config_variable_t var) { setValue(var.name, var.value); }
    void addToConfig(std::string name, std::string value);
    void addToConfig(config_variable_t var) { addToConfig(var.name, var.value); }
    void removeConfig(std::string name);

    sigslot::signal1<std::string> readingFile;
    sigslot::signal1<std::string> cannotReadFile;
    sigslot::signal1<std::string> identicalSourcing;
    sigslot::signal2<std::string, std::string> configChanged;

  protected:
    std::string configFilePath;
    std::list<config_variable_t> config;
};

#endif // CONFIGMANAGER_H
