﻿/*
 * This file is part of babble-curses - curses client for the Babble Server.
 * Mariano López Minnucci <mlopezviedma@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WINDOW_H
#define WINDOW_H

#define SIGSLOT_DEFAULT_MT_POLICY multi_threaded_local

#include <string>
#include <string.h>
#include <libintl.h>
#include <list>
#include <cwchar>
#include <ncurses.h>
#include <clocale>
#include "sigslot.h"

class Window : public sigslot::has_slots<>
{
  public:
    Window(int id, int nlines, int ncols, int begin_y, int begin_x,
           bool has_border = false, int buffer_size = 0);
    ~Window();
    void showWin();
    void refreshSize(int nlines, int ncols, int begin_y, int begin_x);
    void addLine(std::string line);
    void setBufferSize(int buffer_size);
    void clear();
    void scrollUp(int n);
    int getID() const { return id; };
    int getLines() const { return lines; };
    int getCols() const { return cols; };
    int getScroll() const { return scroll; };
    int getMaxScroll();
    void nextArg(std::string &string, std::string &arg);
    
    std::string cmdline, prompt, title, last_message;
    int cursor, offset;
    
    static const std::string RESET;
    static const std::string BOLD;
    static const std::string ITALIC;
    static const std::string UNDERLINE;
    static const std::string NOTBOLD;
    static const std::string NOTITALIC;
    static const std::string NOTUNDERLINE;
    static const std::string RED;
    static const std::string GREEN;
    static const std::string YELLOW;
    static const std::string BLUE;
    static const std::string MAGENTA;
    static const std::string CYAN;
    static const std::string WHITE;
    static const std::string POSITIVE;
    static const std::string NEGATIVE;
    
  protected:
    WINDOW *window;
    WINDOW *border;
    std::list<std::string> content, sized_content;
    int id, lines, cols, total_lines, total_cols, scroll, buffer_size;
    bool has_border;
};

#endif // WINDOW_H
